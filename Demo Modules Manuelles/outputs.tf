output "aws_ami_id" {
    value = data.aws_ami".myapp-ami.id 
} 
output "ec2_public_ip" {
    value = aws_instance.myapp-instance.public_ip
}