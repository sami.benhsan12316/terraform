resource "aws_subnet" "myapp-subnet-1" {
    vpc_id = var.vpc_id 
    cidr_block = var.subnet_cidr_block
    availability_zone = var.availability_zone
    tags = { 
        Name : "${var.env-prefix}-subnet"
    }
}

resource "aws_internet_gateway" "myapp-gateway" {
    vpc_id = var.vpc_id 
    tags = { 
        Name : "${var.env-prefix}-gateway"
    }
}

resource "aws_route_table" "myapp-route-table" {
    vpc_id = var.vpc_id 
    route = {
        cidr_block = "0.0.0.0/0" 
        gateway_id = aws_internet_gateway.myapp-gateway.id
    }
     tags = { 
        Name : "${var.env-prefix}-rte"
    }
}