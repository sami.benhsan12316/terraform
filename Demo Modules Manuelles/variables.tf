variable "vpc_cidr_block" {}
variable "subnet_cidr_block" {}
variable "env-prefix" {}
variable "availability_zone" {}
variable "instance_type" {}