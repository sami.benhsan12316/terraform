provider "aws" {
  region = "eu-west-3"
}

variable "vpc_cidr_bloc" {
  description = "Le cidr block du vpc"
}

variable "subnet_cidr_bloc" {
  description = "Le cidr block du vpc"
}

variable "prefix" {
  description = "Le préfix des variables"
}

resource "aws_vpc" "aws_vpc" {
  cidr_block = var.subnet_cidr_bloc
  tags = {
    Name = "${var.prefix}-vpc"
  }
}

resource "aws_subnet" "sbh-subnet" {
  vpc_id = aws_vpc.aws_vpc.id
  cidr_block = var.subnet_cidr_bloc
  tags = {
    Name = "${var.prefix}-subnet"
  }
}

resource "aws_internet_gateway" "sbh-igw" {
  vpc_id = aws_vpc.aws_vpc.id
  tags = {
    Name = "${var.prefix}-igw"
  }
}

resource "aws_route_table" "sbh-rtb" {
  vpc_id = aws_vpc.aws_vpc.id
  route  {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.sbh-igw.id
  }
  tags = {
    Name = "${var.prefix}-rtb"
  }
}

resource "aws_route_table_association" "rtb-ass" {
  subnet_id = aws_subnet.sbh-subnet.id
  route_table_id = aws_route_table.sbh-rtb.id
}