provider "aws" {
    region = var.region
}

variable region {}
variable vpc_cidr_block {}
variable private_subnets_cidr_block {}
variable public_subnets_cidr_block {}

data "aws_availability_zone" "azs" {}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"      # Voir RQ 1 
  version = "3.19.0"
  # Insert the requiered Arguments # NO HARD CODING ! 
  name = "myapp-vpc" # Esm vpc li bch nhot fih EKS Cluster 
  cidr = var.vpc_cidr_block # Cidr block ta3 vpc ..

  private_subnets = var.private_subnets_cidr_block  # Array ta3 cidrs des subnets (privé & public).. # Voir RQ 2 
  public_subnets = var.public_subnets_cidr_block
  azs =  data.aws_availability_zone.azs.names # Voir RQ 3 + 4 
  
  enable_nat_gateway = true #  Bch yactivi les NAT gateways dans les vpc (malgré enou par default c'est true .. pour la transparence wkhw ) .. Question 10 
  single_nat_gateway = true # Bch tasna3 NAT gateway partagé bin les subnets ycomminikiw à traverou m3a net
  enable_dns_hostnames = true # Bch ki tétsna3 VM EC2 .. yétfféktélha @dns automatiquement

  tags = { # Voir RQ 5 
    "kubernetes.io/cluster/myapp-cluster" = "shared" 
  } 
  public_subnet_tags = {
    "kubernetes.io/cluster/myapp-cluster" = "shared"
    "kubernetes.io/cluster/elb" = 1 
  }
  private_subnet_tags = {
    "kubernetes.io/cluster/myapp-cluster" = "shared"
    "kubernetes.io/cluster/internal-elb" = 1 
  }

}










